*** Settings ***
Library    SeleniumLibrary
Library    String


Resource    ../keyword/common.robot
# feature
Resource    ../keyword/feature/coinmarketcap_main_feature.robot
Resource    ../keyword/feature/bitcoin_feature.robot
# page
Resource    ../keyword/page/coinmarketcap_main_page.robot
Resource    ../keyword/page/bitcoin_page.robot