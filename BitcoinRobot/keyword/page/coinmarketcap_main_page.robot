*** Variables ***
${coinmarketcap_main_page.search_button}    xpath=//div[contains(text(), 'Search')]
${coinmarketcap_main_page.itemList}         xpath=//div[@class="sc-ef9ffd02-0 jFxCnJ"]
${coinmarketcap_main_page.fillBitcoin}      xpath=//input[@type="text"]
${coinmarketcap_main_page.bitcoin}          xpath=//p[contains(text(), 'Bitcoin')]

*** Keywords ***
Click search
    SeleniumLibrary.Wait Until Element Is Visible    ${coinmarketcap_main_page.search_button}
    SeleniumLibrary.Click Element                    ${coinmarketcap_main_page.search_button}

Check item list
    SeleniumLibrary.Wait Until Element Is Visible    ${coinmarketcap_main_page.itemList}

Fill "Bitcoin" in search box
    [Arguments]                                      ${bitcoin}
    Sleep                                            1s
    SeleniumLibrary.Wait Until Element Is Visible    ${coinmarketcap_main_page.fillBitcoin} 
    SeleniumLibrary.Input text                       ${coinmarketcap_main_page.fillBitcoin}     ${bitcoin}

Click search "Bitcoin"
    Sleep                                            1s
    SeleniumLibrary.Wait Until Element Is Visible    ${coinmarketcap_main_page.bitcoin}
    SeleniumLibrary.Click Element                    ${coinmarketcap_main_page.bitcoin}