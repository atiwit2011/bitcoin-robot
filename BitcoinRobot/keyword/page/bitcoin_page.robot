*** Variables ***
${bitcoin_page.bitcoin_logo}    xpath=//img[@src="https://s2.coinmarketcap.com/static/img/coins/64x64/1.png"]
${bitcoin_page.bitcoin_text}    xpath=//span[@data-text="Bitcoin"]/span[contains(text(), 'Bitcoin')]

*** Keywords ***
Check bitcoin logo
    SeleniumLibrary.Wait Until Element Is Visible    ${bitcoin_page.bitcoin_logo}

Check bitcoin text
    [Arguments]                                      ${bitcoin}
    SeleniumLibrary.Wait Until Element Is Visible    ${bitcoin_page.bitcoin_text}
    SeleniumLibrary.Element Should Contain           ${bitcoin_page.bitcoin_text}    ${bitcoin}